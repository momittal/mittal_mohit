/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Organization.DonorOrganization;
import Business.Organization.NurseOrganization;
import Business.Organization.Organization.Type;
import Business.Organization.ReceptionistOrganization;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Mohit
 */
public class AddRoles {
    
    private ReceptionistOrganization receptionistOrg;
    private DonorOrganization donorOrg;
    private NurseOrganization nurseOrg;
    public AddRoles(Type type) {
        if (type.getValue().equals(Type.Receptionist.getValue())){
            ArrayList<Role> roles = new ArrayList<>();
            roles.add(new ReceptionistRole());
        }
        else if (type.getValue().equals(Type.Donor.getValue())){
            Role role = new DonorRole();
            this.donorOrg.getSupportedRole().add(role);
        }
        else if (type.getValue().equals(Type.Nurse.getValue())){
            Role role = new NurseRole();
            this.nurseOrg.getSupportedRole().add(role);
        }
        else{
            JOptionPane.showMessageDialog(null, type.getValue());
        }
    }
}
