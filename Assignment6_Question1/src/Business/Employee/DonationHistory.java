/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import java.util.ArrayList;

/**
 *
 * @author Mohit
 */
public class DonationHistory {
    
    private ArrayList<Donation> donationList;
    
    public DonationHistory() {
        this.donationList = new ArrayList<>();
    }
    
    public Donation addDonation() {
        Donation donation = new Donation();
        donationList.add(donation);
        return donation;
    }
    
    public void deleteDonation(Donation donation) {
        donationList.remove(donation);
    }

    public ArrayList<Donation> getDonationList() {
        return donationList;
    }

    public void setDonationList(ArrayList<Donation> donationList) {
        this.donationList = donationList;
    }
    


}
