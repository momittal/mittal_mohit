/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

/**
 *
 * @author Mohit
 */
public class Donor extends Employee{
    private DonationHistory donationHistory;
    
    public Donor() {
        this.donationHistory = new DonationHistory();
    }
    public DonationHistory getDonationHistory() {
        return donationHistory;
    }

    public void setDonationHistory(DonationHistory donationHistory) {
        this.donationHistory = donationHistory;
    }
    
}
