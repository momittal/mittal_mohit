/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import java.util.Date;

/**
 *
 * @author Mohit
 */
public class Donation {
    private int barcode;
    private Date dateOfExtraction;
    private String bloodType;
    private String nurse;
    private static int id;
    private int count = 0;
    
    public Donation(){
        count++;
        id = count;
    }
    
    public int getBarcode() {
        return barcode;
    }

    public void setBarcode(int barcode) {
        this.barcode = barcode;
    }

    public Date getDateOfExtraction() {
        return dateOfExtraction;
    }

    public void setDateOfExtraction(Date dateOfExtraction) {
        this.dateOfExtraction = dateOfExtraction;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getNurse() {
        return nurse;
    }

    public void setNurse(String Nurse) {
        this.nurse = Nurse;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }
    
    
}
