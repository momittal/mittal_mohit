package BusinessConfigure;


import Business.ConfigureASystem;
import Business.EcoSystem;
import Business.Employee.Employee;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mohit
 */
public class ConfigureEcoSystem_2 {
        public static EcoSystem config(){
        EcoSystem ecosystem = ConfigureASystem.configure();
        
        
        
        
        
        Employee employee = ecosystem.getEmployeeDirectory().createEmployee("Mohit");
        UserAccount ua = ecosystem.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", employee, new SystemAdminRole());
        
        return ecosystem;
        }
    
}
