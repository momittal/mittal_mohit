/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mohit
 */
public class Quiz extends HttpServlet {

    private String[] userResponse = new String[10];
    private ArrayList<Question> questionList = new ArrayList<Question>();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Quiz</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Quiz at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Quiz</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<form name='start' method='post' action='ques1'>");
        out.println("<input type='hidden' value='1' name='quesNum'/>");
        out.println("<input type='submit' name='submit' value='Start Quiz'/>");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        questionList = getQuestionList();
        int quesNum = Integer.parseInt(request.getParameter("quesNum"));
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Quiz</title>");
        out.println("</head>");
        out.println("<body>");
        switch (quesNum) {
            case 1:
                out.println("<form name='ques1' method='post' action='ques2'>");
                out.println("<label>Question 1: </label>");
                out.println(questionList.get(0).getQuestion());
                out.println("<br/><input type='radio' name='options1' value='" + questionList.get(0).getOption1() + "'>" + questionList.get(0).getOption1());
                out.println("<br/><input type='radio' name='options1' value='" + questionList.get(0).getOption2() + "'>" + questionList.get(0).getOption2());
                out.println("<br/><input type='radio' name='options1' value='" + questionList.get(0).getOption3() + "'>" + questionList.get(0).getOption3());
                out.println("<br/><input type='radio' name='options1' value='" + questionList.get(0).getOption4() + "'>" + questionList.get(0).getOption4());
                out.println("<br/><input type='hidden' value='2' name='quesNum'/>");
                out.println("<br/><input type='submit' name='submit' value='Next'/>");
                out.println("</form>");
                break;
            case 2:
                userResponse[0] = request.getParameter("options1");
                out.println(printQuestion(2));
                
                break;
            case 3:
                userResponse[1] = request.getParameter("options2");
                out.println(printQuestion(3));
                break;

            case 4:
                userResponse[2] = request.getParameter("options3");
                out.println(printQuestion(4));
                break;

            case 5:
                userResponse[3] = request.getParameter("options4");
                out.println(printQuestion(5));
                break;
            case 6:
                userResponse[4] = request.getParameter("options5");
                out.println(printQuestion(6));
                break;
            case 7:
                userResponse[5] = request.getParameter("options6");
                out.println(printQuestion(7));
                break;
            case 8:
                userResponse[6] = request.getParameter("options7");
                out.println(printQuestion(8));
                break;
            case 9:
                userResponse[7] = request.getParameter("options8");
                out.println(printQuestion(9));
                break;
            case 10:
                userResponse[8] = request.getParameter("options9");
                out.println(printQuestion(10));
                break;
            case 11:
                userResponse[9] = request.getParameter("options10");
                int correctAnswers = 0;
                String[] correctAnswer = new String[10];

                for (int i = 0; i < 10; i++) {
                    if (questionList.get(i).getCorrectAnswer().equals(userResponse[i])) {
                        correctAnswers++;
                    }
                }
                out.println("Result: " + correctAnswers + "/10");
                for (int i = 0; i < 10; i++) {
                    out.println("<br/><br/>Question " + (i + 1) + ": " + questionList.get(i).getQuestion());
                    out.println("<br/>Your Answer: " + userResponse[i] + "<br/>");
                    out.println("Correct Answer: " + questionList.get(i).getCorrectAnswer());
                }

                break;
        }

        out.println("</body>");
        out.println("</html>");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static ArrayList<Question> getQuestionList() {
        ArrayList<Question> questionList = new ArrayList<Question>();
        Question ques1 = new Question();
        ques1.setQuestion("Which life cycle method is called once in servlet");
        ques1.setCorrectAnswer("init()");
        ques1.setOption1("class loading");
        ques1.setOption2("init()");
        ques1.setOption3("destroy()");
        ques1.setOption4("service()");
        questionList.add(ques1);

        Question ques2 = new Question();
        ques2.setQuestion("Which of these life-cycle method you can over-ride in your class");
        ques2.setCorrectAnswer("All of these");
        ques2.setOption1("init()");
        ques2.setOption2("service()");
        ques2.setOption3("destroy()");
        ques2.setOption4("All of these");
        questionList.add(ques2);

        Question ques3 = new Question();
        ques3.setQuestion("Which statement is not true about ServletContext");
        ques3.setCorrectAnswer("There is one ServletContext per one servlet");
        ques3.setOption1("There is one ServletContext per one application");
        ques3.setOption2("Generally it is used to get web app parameters");
        ques3.setOption3("We can get Server Information through it");
        ques3.setOption4("There is one ServletContext per one servlet");
        questionList.add(ques3);

        Question ques4 = new Question();
        ques4.setQuestion("Which statement is not true about ServletConfig");
        ques4.setCorrectAnswer("There is one servlet config per one application");
        ques4.setOption1("There is one servlet config per one application");
        ques4.setOption2("We can access ServletContext through it");
        ques4.setOption3("Provide deploy-time information to server");
        ques4.setOption4("There is one servlet config per one servlet");
        questionList.add(ques4);

        Question ques5 = new Question();
        ques5.setQuestion("Which method is called when client request come?");
        ques5.setCorrectAnswer("service()");
        ques5.setOption1("init()");
        ques5.setOption2("service()");
        ques5.setOption3("get()");
        ques5.setOption4("post()");
        questionList.add(ques5);

        Question ques6 = new Question();
        ques6.setQuestion("Which interface contain servlet life-cycle methods?");
        ques6.setCorrectAnswer("Servlet");
        ques6.setOption1("HttpServlet");
        ques6.setOption2("GenericServlet");
        ques6.setOption3("Service");
        ques6.setOption4("Servlet");
        questionList.add(ques6);

        Question ques7 = new Question();
        ques7.setQuestion("Which life-cycle method make ready the servlet for garbage collection");
        ques7.setCorrectAnswer("destroy()");
        ques7.setOption1("init");
        ques7.setOption2("service");
        ques7.setOption3("destroy()");
        ques7.setOption4("system.gc");
        questionList.add(ques7);

        Question ques8 = new Question();
        ques8.setQuestion("Which method does not exists in HttpServlet Class");
        ques8.setCorrectAnswer("init");
        ques8.setOption1("init");
        ques8.setOption2("service");
        ques8.setOption3("doGet");
        ques8.setOption4("doPost");
        questionList.add(ques8);

        Question ques9 = new Question();
        ques9.setQuestion("Which http method is idempotent");
        ques9.setCorrectAnswer("get");
        ques9.setOption1("get");
        ques9.setOption2("post");
        ques9.setOption3("trace");
        ques9.setOption4("option");
        questionList.add(ques9);

        Question ques10 = new Question();
        ques10.setQuestion("Which tag of DD maps internal name of servlet to public URL pattern");
        ques10.setCorrectAnswer("servlet-mapping");
        ques10.setOption1("servlet");
        ques10.setOption2("servlet-mapping");
        ques10.setOption3("web-app");
        ques10.setOption4("service-mappings");
        questionList.add(ques10);

        return questionList;
    }

    public String printQuestion(int caseNum) {
        String html = "";
        html += "<form name='ques" + caseNum + "' method='post' action='";
        if(caseNum!=10){
           html += "ques"+ (caseNum + 1); 
        }else{
            html+= "finish";
        }
        html+= "'>";
        html += "<label>Question " + caseNum + ": </label>";
        html += questionList.get(caseNum - 1).getQuestion();
        html += "<br/><input type='radio' name='options" + caseNum + "' value='" + questionList.get(caseNum - 1).getOption1() + "'>" + questionList.get(caseNum - 1).getOption1();
        html += "<br/><input type='radio' name='options" + caseNum + "' value='" + questionList.get(caseNum - 1).getOption2() + "'>" + questionList.get(caseNum - 1).getOption2();
        html += "<br/><input type='radio' name='options" + caseNum + "' value='" + questionList.get(caseNum - 1).getOption3() + "'>" + questionList.get(caseNum - 1).getOption3();
        html += "<br/><input type='radio' name='options" + caseNum + "' value='" + questionList.get(caseNum - 1).getOption4() + "'>" + questionList.get(caseNum - 1).getOption4();
        html += "<br/><input type='hidden' value='" + (caseNum + 1) + "' name='quesNum'/>";
        html += "<br/><input type='submit' name='submit' value='Next'/>";
        html += "</form>";
        return html;
    }
}
