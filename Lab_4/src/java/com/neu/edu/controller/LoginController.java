/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.edu.controller;

import com.neu.edu.beans.MessageBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mohit
 */
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String value = request.getParameter("action");
            if (value.equals("logout")) {
                HttpSession session = request.getSession();
                session.invalidate();
                response.sendRedirect("index.jsp");
            }

            if (!value.equals("reply")) {
                HttpSession session = request.getSession();
                if (session.getAttribute("userName") != null) {
                    RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Views/UserView.jsp");
                    rd.forward(request, response);
                }

                String userName = request.getParameter("userName");
                String password = request.getParameter("password");

                Connection conn = establishConnectionJDBC();
                String queryLogin = "select * from usertable where UserName=? and UserPassword=?";
                PreparedStatement loginStmt = conn.prepareStatement(queryLogin);
                loginStmt.setString(1, userName);
                loginStmt.setString(2, password);

                ResultSet resultSet = loginStmt.executeQuery();

                if (resultSet.next()) {
                    String quesryMessages = "select * from messages where UserName=?";
                    PreparedStatement ps = conn.prepareStatement(quesryMessages);
                    ps.setString(1, userName);
                    ResultSet resultMessage = ps.executeQuery();

                    ArrayList<MessageBean> messageBeanList = new ArrayList<>();
                    while (resultMessage.next()) {
                        MessageBean messageBean = new MessageBean();
                        messageBean.setFromUser(resultMessage.getString("fromUser"));
                        messageBean.setMessage(resultMessage.getString("message"));
                        messageBean.setUserName(resultMessage.getString("userName"));
                        messageBean.setMessageID(resultMessage.getString("messageID"));
                        messageBean.setMessageDate(resultMessage.getString("messageDate"));

                        messageBeanList.add(messageBean);

                    }

                    session.setAttribute("userName", userName);
                    session.setAttribute("messageList", messageBeanList);

                    //create cookie
                    Cookie userCookie = new Cookie("userName", request.getParameter
("userName"));
                    Cookie passCookie = new Cookie("password", request.getParameter
("password"));

                    response.addCookie(passCookie);
                    response.addCookie(userCookie);

                    RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Views/UserView.jsp");
                    rd.forward(request, response);
                } else {
                    //redirect for invalid credentials
                    RequestDispatcher rd = request.getRequestDispatcher("/index.jsp?error=true");
                    rd.forward(request, response);
                }
            }else{
                RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/Views/UserView.jsp");
                rd.forward(request, response);
            }
        }
    }

    protected Connection establishConnectionJDBC() throws IOException {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver"); //load Driver
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MYSQL JDBC Driver");
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection
("jdbc:mysql://newton.neu.edu:3306/usersdb", "student", "p@ssw0rd");
        } catch (SQLException e) {
            System.out.println("Connection failed!");
            e.printStackTrace();
        }
        if (connection != null) {
            System.out.println("You made it, take control of your database");
        }
        return connection;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
