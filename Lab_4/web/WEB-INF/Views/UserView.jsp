<%-- 
    Document   : UserView
    Created on : Feb 6, 2016, 11:19:46 AM
    Author     : Mohit
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User View</title>
    </head>
    <body>
        <h1>Welcome <c:out value ="${sessionScope.userName}"></c:out></h1>
        <table border="1">
            <tr>
                <th>From</th>
                <th>Message</th>
                <th></th>
                <th>Message Date</th>
                <th></th>
            </tr>
        <c:forEach var="message" items="{sessionScope.messagelist}">
            <tr>
                <td><c:out value="${message.fromUser}"></c:out></td>
                <td><c:out value="${message.message}"></c:out></td>
                <td><a href = "reply.htm?tp=<c:out value="${message.fromUser}"></c:out>&action=reply">Reply </a></td>
                <td><c:out value="${message.messageDate}"/></td>
                <td><a href="">Delete</a></td>
            </tr>
        </c:forEach>
        </table>
    </body>
</html>
