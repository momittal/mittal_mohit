<%-- 
    Document   : reply
    Created on : Feb 6, 2016, 11:58:45 AM
    Author     : Mohit
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="menu.jsp"/>
        <h1>Welcome <c:out value =  "{sessionScope.userName}"/>!</h1>
        <form action="sendMessage.htm" method ="post">
            <h3>From: </h3><c:out value =  "{sessionScope.userName}"/>  
            <h3>To: </h3><c:out value =  "{param.to}"/> 
            <h3>Message</h3>
            <textarea name="message" rows="6" cols="10"></textarea>
            <br/>
            <input type="submit" value="Send"/>
            <input type="hidden" name="to" value="${param.to}"/>
        </form>
        
    </body>
</html>
