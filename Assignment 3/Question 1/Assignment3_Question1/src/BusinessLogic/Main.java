package BusinessLogic;

import java.util.ArrayList;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author m
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        SupplierDirectory supplierList = new SupplierDirectory();

        String splrName1, splrName2, splrName3, splrName4, splrName5;

        splrName1 = "Dell";
        splrName2 = "Lenovo";
        splrName3 = "Apple";
        splrName4 = "Sony";
        splrName5 = "Samsung";

        Supplier splr1 = supplierList.addSupplier();
        Supplier splr2 = supplierList.addSupplier();
        Supplier splr3 = supplierList.addSupplier();
        Supplier splr4 = supplierList.addSupplier();
        Supplier splr5 = supplierList.addSupplier();

        splr1.setSupplierName(splrName1);
        splr2.setSupplierName(splrName2);
        splr3.setSupplierName(splrName3);
        splr4.setSupplierName(splrName4);
        splr5.setSupplierName(splrName5);

        ProductCatalog proCat1 = splr1.getProductCatalog();
        proCat1.addProduct().setProductName("Product 1");
        proCat1.addProduct().setProductName("Product 2");
        proCat1.addProduct().setProductName("Product 3");
        proCat1.addProduct().setProductName("Product 4");
        proCat1.addProduct().setProductName("Product 5");
        proCat1.addProduct().setProductName("Product 6");
        proCat1.addProduct().setProductName("Product 7");
        proCat1.addProduct().setProductName("Product 8");
        proCat1.addProduct().setProductName("Product 9");
        proCat1.addProduct().setProductName("Product 10");

        ProductCatalog proCat2 = splr2.getProductCatalog();
        proCat2.addProduct().setProductName("Product 1");
        proCat2.addProduct().setProductName("Product 2");
        proCat2.addProduct().setProductName("Product 3");
        proCat2.addProduct().setProductName("Product 4");
        proCat2.addProduct().setProductName("Product 5");
        proCat2.addProduct().setProductName("Product 6");
        proCat2.addProduct().setProductName("Product 7");
        proCat2.addProduct().setProductName("Product 8");
        proCat2.addProduct().setProductName("Product 9");
        proCat2.addProduct().setProductName("Product 10");

        ProductCatalog proCat3 = splr3.getProductCatalog();
        proCat3.addProduct().setProductName("Product 1");
        proCat3.addProduct().setProductName("Product 2");
        proCat3.addProduct().setProductName("Product 3");
        proCat3.addProduct().setProductName("Product 4");
        proCat3.addProduct().setProductName("Product 5");
        proCat3.addProduct().setProductName("Product 6");
        proCat3.addProduct().setProductName("Product 7");
        proCat3.addProduct().setProductName("Product 8");
        proCat3.addProduct().setProductName("Product 9");
        proCat3.addProduct().setProductName("Product 10");

        ProductCatalog proCat4 = splr4.getProductCatalog();
        proCat4.addProduct().setProductName("Product 1");
        proCat4.addProduct().setProductName("Product 2");
        proCat4.addProduct().setProductName("Product 3");
        proCat4.addProduct().setProductName("Product 4");
        proCat4.addProduct().setProductName("Product 5");
        proCat4.addProduct().setProductName("Product 6");
        proCat4.addProduct().setProductName("Product 7");
        proCat4.addProduct().setProductName("Product 8");
        proCat4.addProduct().setProductName("Product 9");
        proCat4.addProduct().setProductName("Product 10");

        ProductCatalog proCat5 = splr5.getProductCatalog();
        proCat5.addProduct().setProductName("Product 1");
        proCat5.addProduct().setProductName("Product 2");
        proCat5.addProduct().setProductName("Product 3");
        proCat5.addProduct().setProductName("Product 4");
        proCat5.addProduct().setProductName("Product 5");
        proCat5.addProduct().setProductName("Product 6");
        proCat5.addProduct().setProductName("Product 7");
        proCat5.addProduct().setProductName("Product 8");
        proCat5.addProduct().setProductName("Product 9");
        proCat5.addProduct().setProductName("Product 10");

        System.out.println("Supplier and Product Catalog");

        for (Supplier splr : supplierList.getSupplierDirectory()) {

            ProductCatalog prodCatalog = splr.getProductCatalog();

            for (Product pro : prodCatalog.getProductList()) {
                System.out.println("________________________");
                System.out.println(splr.getSupplierName() + " " + pro.getProductName());
            }

        }

    }

}
