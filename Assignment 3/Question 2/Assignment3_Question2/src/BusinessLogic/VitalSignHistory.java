/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author m
 */
public class VitalSignHistory {
    private ArrayList<VitalSign> vitalSignHistory;             //<VitalSign> will only allow object of VitalSign only. This is called generics
    
    public VitalSignHistory() {
        vitalSignHistory = new ArrayList<>();
    }

    public ArrayList<VitalSign> getVitalSignHistory() {
        return vitalSignHistory;
    }

    public void setVitalSignHistory(ArrayList<VitalSign> vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }
    
    public VitalSign addVitalSign() {
        VitalSign vs = new VitalSign();
        vitalSignHistory.add(vs);       //new object vs is added everytime this line runs
        return vs;
    }
    
    public void deleteVitalSign(VitalSign vs){
        vitalSignHistory.remove(vs);    //everytime this line runs object vs is removed
    }
    
}
