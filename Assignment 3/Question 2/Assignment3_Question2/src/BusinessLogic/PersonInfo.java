/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author m
 */
public class PersonInfo {
    private String personName;
    private int personAge;
    private int personID;
    private String personAddress;
    private int personPhone;
    
    private PatientList patientDirectory;
    private VitalSignHistory vitalSignHistory;
    
    static int count = 0;          //if count is not kept static then evevrytime a new product is created the count will start from 0
    
    public PersonInfo(){
             this.patientDirectory = new PatientList();
             this.vitalSignHistory = new VitalSignHistory();
        
        count++;
        personID = count;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        PersonInfo.count = count;
    }

    public VitalSignHistory getVitalSignHistory() {
        return vitalSignHistory;
    }

    public void setVitalSignHistory(VitalSignHistory vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }

    
    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public int getPersonAge() {
        return personAge;
    }

    public void setPersonAge(int personAge) {
        this.personAge = personAge;
    }

    public int getPersonID() {
        return personID;
    }

    public void setPersonID(int personID) {
        this.personID = personID;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }

    public int getPersonPhone() {
        return personPhone;
    }

    public void setPersonPhone(int personPhone) {
        this.personPhone = personPhone;
    }

    public PatientList getPatientDirectory() {
        return patientDirectory;
    }

    public void setPatientDirectory(PatientList patientDirectory) {
        this.patientDirectory = patientDirectory;
    }

    

   
    
    
    
    @Override
    public String toString(){
        return personName;
    }
    
}
