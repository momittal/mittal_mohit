/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author m
 */
public class PersonList {
    private ArrayList<PersonInfo> personList;
    
    public PersonList(){
        personList = new ArrayList<PersonInfo>();
    }

    public ArrayList<PersonInfo> getPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<PersonInfo> personList) {
        this.personList = personList;
    }
    
    public PersonInfo addPerson(){
        PersonInfo person = new PersonInfo();
        personList.add(person);
        return person;
    }
    
    public void removePerosn(PersonList person){
        personList.remove(person);
    }
    
    public PersonInfo searchPerson(String personName){
        for (PersonInfo person: personList){
            if(person.getPersonName().equalsIgnoreCase(personName)){
                return person;
            }
        }
        return null;
    }
    
    
}
