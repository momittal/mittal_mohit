/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.Date;

/**
 *
 * @author m
 */
public class PatientInfo {
    private String patientName;
    private int patientID;
    private String preferredDoctor;
    private String preferredPharmacy;
    static int count = 100;       //if count is not kept static then evevrytime a new product is created the count will start from 0
     private VitalSignHistory vitalSignHistory;
    
    public PatientInfo(){
        
         this.vitalSignHistory = new VitalSignHistory();
         count++;
        patientID = count;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    
    
    
    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public String getPreferredDoctor() {
        return preferredDoctor;
    }

    public void setPreferredDoctor(String preferredDoctor) {
        this.preferredDoctor = preferredDoctor;
    }

    public String getPreferredPharmacy() {
        return preferredPharmacy;
    }

    public void setPreferredPharmacy(String preferredPharmacy) {
        this.preferredPharmacy = preferredPharmacy;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        PatientInfo.count = count;
    }

    public VitalSignHistory getVitalSignHistory() {
        return vitalSignHistory;
    }

    public void setVitalSignHistory(VitalSignHistory vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }

    
 
    
     
    
    
}
