/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author m
 */
public class PatientList {
    private ArrayList<PatientInfo> patientList;
    
    public PatientList(){
        patientList = new ArrayList<PatientInfo>();
    }
    
    public ArrayList<PatientInfo> getPatientList() {
        return patientList;
    }

    public void setPatientList(ArrayList<PatientInfo> patientList) {
        this.patientList = patientList;
    }
    
    public PatientInfo addPatient(){
        PatientInfo patient = new PatientInfo();
        patientList.add(patient);
        return patient;
    }
    
    public void removePatient(PatientList patient){
        patientList.remove(patient);
    }
    
   
}
