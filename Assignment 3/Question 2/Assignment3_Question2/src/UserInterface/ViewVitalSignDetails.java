/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import BusinessLogic.PatientInfo;
import BusinessLogic.PersonInfo;
import BusinessLogic.VitalSign;
import BusinessLogic.VitalSignHistory;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author m
 */
public class ViewVitalSignDetails extends javax.swing.JPanel {
    private JPanel userProcessContainer;
    private PatientInfo patient;
    private ViewPatientDetailsJPanel vpdjp;
    private PersonInfo person;
    private VitalSignHistory vitalSignHistory;

    /**
     * Creates new form ViewVitalSignDetails
     */
    public ViewVitalSignDetails(PatientInfo patient,JPanel userProcessContainer,ViewPatientDetailsJPanel vpdjp,VitalSignHistory vitalSignHistory,PersonInfo person ) {
        initComponents();
        this.patient = patient;
        this.userProcessContainer = userProcessContainer;
        this.vpdjp = vpdjp;
        this.vitalSignHistory = vitalSignHistory;
        this.patient = patient;
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        respiratoryRateTxtField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        heartRateTxtField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        systolicBPTxtField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        weightTxtField = new javax.swing.JTextField();
        saveVitalSignBtn = new javax.swing.JButton();

        jLabel2.setText("Respiratory Rate");

        respiratoryRateTxtField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                respiratoryRateTxtFieldActionPerformed(evt);
            }
        });

        jLabel3.setText("Heart Rate");

        jLabel4.setText("Systolic Blood Pressure");

        jLabel5.setText("Weight (lbs)");

        saveVitalSignBtn.setText("SAVE");
        saveVitalSignBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveVitalSignBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(211, 211, 211)
                .addComponent(saveVitalSignBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(212, 212, 212))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(144, 144, 144)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel5)
                        .addComponent(jLabel4)
                        .addComponent(jLabel3)
                        .addComponent(jLabel2))
                    .addGap(25, 25, 25)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(heartRateTxtField, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                        .addComponent(respiratoryRateTxtField)
                        .addComponent(systolicBPTxtField)
                        .addComponent(weightTxtField))
                    .addContainerGap(89, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(325, Short.MAX_VALUE)
                .addComponent(saveVitalSignBtn)
                .addGap(132, 132, 132))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(173, 173, 173)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(respiratoryRateTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(heartRateTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(systolicBPTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(weightTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(173, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void respiratoryRateTxtFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_respiratoryRateTxtFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_respiratoryRateTxtFieldActionPerformed

    private void saveVitalSignBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveVitalSignBtnActionPerformed
        
            try{
                VitalSign vitalSign;
                vitalSign = vitalSignHistory.addVitalSign();
                vitalSign.setRespiratoryRate(Integer.parseInt(respiratoryRateTxtField.getText().trim()));
                vitalSign.setHeartRate(Integer.parseInt(heartRateTxtField.getText().trim()));
                vitalSign.setSystolicBP(Integer.parseInt(systolicBPTxtField.getText().trim()));
                vitalSign.setWeight(Integer.parseInt(weightTxtField.getText().trim()));
                 Date date = new Date();
                vitalSign.setDate(date);
                JOptionPane.showMessageDialog(null, "Vital Sign object updated succeefully");
                
            }
            catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null,
                    "Error: You must enter numbers in the fields");
            }
        

    }//GEN-LAST:event_saveVitalSignBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField heartRateTxtField;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField respiratoryRateTxtField;
    private javax.swing.JButton saveVitalSignBtn;
    private javax.swing.JTextField systolicBPTxtField;
    private javax.swing.JTextField weightTxtField;
    // End of variables declaration//GEN-END:variables
}
