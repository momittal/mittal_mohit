/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.edu;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mohit
 */
public class ServletPart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Children</title>");
        out.println("<script src='children.js'> </script>");
        out.println("</head>");
        out.println("<body>");

        out.println("<form id='form1' name='form1' method='get' action='javascript:submit()'>");
        out.println("How many children do you have?:");
        out.println("<input type='text' id='numOfChildren'/>");
        out.println("<input type='submit'  id='submit' value='submit'/>");
        out.println("</form>");

        out.println("<form id='form2' name='form2' method='post' action='ServletPart'>");
        out.println("<div id='spaceForm2'></div>");
        out.println("</form>");

        out.println("</body>");
        out.println("</html>");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Children Response</title>");
        out.println("</head>");
        out.println("<body>");

        out.println("Name of your children are ");
        for (Enumeration names = request.getParameterNames(); names.hasMoreElements();) {
            String parameterName = (String) names.nextElement();
            if (!parameterName.equalsIgnoreCase("Submit")) {
                String[] parameterValue = request.getParameterValues(parameterName);
                for (String s : parameterValue) {
                    out.println("<br/><br/>");
                    out.println(s);
                }
            }
        }

        out.println("</body>");
        out.println("</html>");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
