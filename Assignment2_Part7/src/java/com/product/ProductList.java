/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.product;

import com.product.Bean.Product;
import com.product.Bean.Computer;
import com.product.Bean.Music;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mohit
 */
public class ProductList extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Product> listOfProducts = new ArrayList<Product>();
        listOfProducts = getProductsAvailable();


        List<Product> outputProducts = new ArrayList<Product>();

        String category = request.getParameter("cat");

        if (category.equalsIgnoreCase("Books")) {
            for (Product product : listOfProducts) {
                if (product.getCategory().equalsIgnoreCase("Books")) {
                    outputProducts.add(product);
                }

            }
        } else if (category.equalsIgnoreCase("Music")) {
            for (Product product : listOfProducts) {
                if (product.getCategory().equalsIgnoreCase("Music")) {
                    outputProducts.add(product);
                }

            }
        } else if (category.equalsIgnoreCase("Computers")) {
            for (Product product : listOfProducts) {
                if (product.getCategory().equalsIgnoreCase("Computers")) {
                    outputProducts.add(product);
                }

            }
        }

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Shopping Store</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<p align='center'>");
            out.println("<a href='ProductList?cat=Books'>Books</a> | ");
            out.println("<a href='ProductList?cat=Computers'>Computers</a> | ");
            out.println("<a href='ProductList?cat=Music'>Music</a>");
            out.println("</p>");
            out.println("<form method='post' action='addProduct.htm'>");
            if (category.equalsIgnoreCase("Books")) {
                out.println("<h5>Shop for Books</h5>");
            }
                
            
                for (Product product : outputProducts) {
                        String price = product.getPrice();
                        String productName = product.getProductName();
                        out.println("<input type='checkbox' name='productName' value='" + productName + "'/>"
                                + productName + " [$" + price + "]<br/>");


                }
          

            out.println("<input type='hidden' name='action' value='multiple'/>");
            out.println("<input type='submit' value='Add to cart'/>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static List<Product> getProductsAvailable() {
        List<Product> productList = new ArrayList<Product>();
        Product p1 = new Product();
        p1.setProductName("Java Programming");
        p1.setPrice("19.99");
        p1.setCategory("Books");

        Product p2 = new Product();
        p2.setProductName("Java Servlet Programming");
        p2.setPrice("29.99");
        p2.setCategory("Books");

        Product p3 = new Product();
        p3.setProductName("Object oriented Programming");
        p3.setPrice("19.99");
        p3.setCategory("Books");

        Product p4 = new Product();
        p4.setProductName("Adele");
        p4.setPrice("100.01");
        p4.setCategory("Music");

        Product p5 = new Product();
        p5.setProductName("Surface Pro");
        p5.setPrice("999.99");
        p5.setCategory("Computers");

        productList.add(p1);
        productList.add(p2);
        productList.add(p3);
        productList.add(p4);
        productList.add(p5);

        return productList;
    }

    

    

}
