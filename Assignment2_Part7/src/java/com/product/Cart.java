/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.product;

import com.product.Bean.Product;
import com.product.Bean.Computer;
import com.product.Bean.Music;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mohit
 */
public class Cart extends HttpServlet {
    
    private List<Product> productList;
    private Set<Product> myProducts;



    public Cart() {
        productList = ProductList.getProductsAvailable();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (null != session.getAttribute("myProductList")) {
            myProducts = (Set<Product>) session.getAttribute("myProductList");
        } else {
            myProducts = new HashSet<Product>();
        }
        try {
            String actionValue = request.getParameter("action");
            if (actionValue.equalsIgnoreCase("multiple")) {
                String[] selectedProduct = request.getParameterValues("productName");
                for (int i = 0; i < selectedProduct.length; i++) {
                    for (Product product : productList) {
                        if (product.getProductName().equals(selectedProduct[i])) {
                            myProducts.add(product);
                        }
                    }
                }
            } else if (actionValue.equalsIgnoreCase("remove")) {
                boolean check = false;
                String removeProduct = request.getParameter("productName");
                Iterator iterator = myProducts.iterator();
                while (iterator.hasNext()) {
                    if (check) {
                        break;
                    }
                    Product product = (Product) iterator.next();
                    if (product.getProductName().equals(removeProduct)) {
                        myProducts.remove(product);
                        check = true;
                    }
                }
            }
            session.setAttribute("myProductList", myProducts);
            response.sendRedirect("ProductView.jsp");
        } catch (Exception e) {
            System.out.println("exception is" + e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
