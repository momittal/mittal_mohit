<%-- 
    Document   : ProductView
    Created on : Feb 3, 2016, 9:43:11 PM
    Author     : Mohit
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.product.Bean.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Cart</title>
    </head>
    <body>
        <h1>Items Currently in your cart</h1>
        <%
            HashSet<Product> myProductSet=new HashSet<Product>();
            myProductSet=(HashSet<Product>)session.getAttribute("myProductList");
            Iterator iterator=myProductSet.iterator();
            int i=0;
            float total=0;%>
            <table>
                
        <%
            while(iterator.hasNext())
            {   
                i++;
                Product product = (Product) iterator.next();
                String productName= product.getProductName();
                String priceString = product.getPrice();
                float price = Float.valueOf(priceString);
                total = total+price;
                
            %>
            <tr><td><jsp:expression>i</jsp:expression></td><td><jsp:expression>productName</jsp:expression> </td><td> <jsp:expression> price </jsp:expression> </td>
                <td><a href='removeBook.htm?action=remove&productName=<jsp:expression>product</jsp:expression>'> Remove From Cart </a></td>                <jsp:scriptlet>}</jsp:scriptlet>
            <tr><td></td><td></td><td><jsp:expression>total</jsp:expression></td></tr>
            </tr></table> 
                <a href='index.html'> Go to Home Page</a>
            
    </body>
</html>
