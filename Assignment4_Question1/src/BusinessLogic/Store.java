/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author m
 */
public class Store {
    private String storeAlias;
    private int storeID;
    private String storeLocation;
    private static int count = 0;
    public DrugInventory drugInventory;

    public Store(){
        count++;
        storeID = count;
        drugInventory = new DrugInventory();
    }
    
    public String getStoreAlias() {
        return storeAlias;
    }

    public void setStoreAlias(String storeAlias) {
        this.storeAlias = storeAlias;
    }

    public int getStoreID() {
        return storeID;
    }

   

    public String getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(String storeLocation) {
        this.storeLocation = storeLocation;
    }

    public DrugInventory getDrugInventory() {
        return drugInventory;
    }

    public void setDrugInventory(DrugInventory drugInventory) {
        this.drugInventory = drugInventory;
    }
    

    @Override
    public String toString() {
        return storeAlias;
    }
    
}
