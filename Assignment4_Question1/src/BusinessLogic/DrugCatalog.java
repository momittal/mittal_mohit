/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author m
 */
public class DrugCatalog {
    private ArrayList<Drug> drugCatalogList;
    
    public DrugCatalog() {
        this.drugCatalogList = new ArrayList<Drug>();
    }

    public ArrayList<Drug> getDrugCatalogList() {
        return drugCatalogList;
    }

    public void setDrugCatalogList(ArrayList<Drug> drugCatalogList) {
        this.drugCatalogList = drugCatalogList;
    }
    
    
    public Drug addDrug(){
        Drug d = new Drug();
        drugCatalogList.add(d);
        return d;
    }
    
    public void removeDrug(Drug d){
        drugCatalogList.remove(d);
    }
    
    
}
