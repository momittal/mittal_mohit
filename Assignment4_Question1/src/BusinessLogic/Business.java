/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;
    
/**
 *
 * @author m
 */
public class Business {
    private StoreDirectory storeDirectory;
    private DrugCatalog drugCatalog;
    
    public Business(){
        this.drugCatalog = new DrugCatalog();
        this.storeDirectory = new StoreDirectory();
        this.drugCatalog = addDrug();
    }

    public StoreDirectory getStoreDirectory() {
        return storeDirectory;
    }

    public void setStoreDirectory(StoreDirectory storeDirectory) {
        this.storeDirectory = storeDirectory;
    }

    public DrugCatalog getDrugCatalog() {
        return drugCatalog;
    }

    public void setDrugCatalog(DrugCatalog drugCatalog) {
        this.drugCatalog = drugCatalog;
    }

    public DrugCatalog addDrug() {
        Drug d1 = drugCatalog.addDrug();
        d1.setDrugName("Amoxicillin");
        d1.setDrugColor("Blue");
        d1.setDrugShape("Oval");
        d1.setGenericName("Amoxicillin");
        d1.setManufacturer("Amoxil");
        d1.setListPrice(6);
        d1.setMaxDosage(2);
        d1.setMinDosage(1);
        d1.setRecommendedDOsage(1);
        
        Drug d2 = drugCatalog.addDrug();
        d2.setDrugName("Ciprofloxacin");
        d2.setDrugColor("White");
        d2.setDrugShape("Oval");
        d2.setGenericName("Ciprofloxacin");
        d2.setManufacturer("Cipro");
        d2.setListPrice(10);
        d2.setMaxDosage(3);
        d2.setMinDosage(1);
        d2.setRecommendedDOsage(2);
        
        
        Drug d3 = drugCatalog.addDrug();
        d3.setDrugName("Acetaminophen");
        d3.setDrugColor("Green");
        d3.setDrugShape("Square");
        d3.setGenericName("Acetaminophen");
        d3.setManufacturer("Acephen");
        d3.setListPrice(10);
        d3.setMaxDosage(3);
        d3.setMinDosage(1);
        d3.setRecommendedDOsage(2);
        
        Drug d4 = drugCatalog.addDrug();
        d4.setDrugName("Cymbalta");
        d4.setDrugColor("White");
        d4.setDrugShape("Oval");
        d4.setGenericName("duloxetine");
        d4.setManufacturer("Cymbalta");
        d4.setListPrice(10);
        d4.setMaxDosage(4);
        d4.setMinDosage(2);
        d4.setRecommendedDOsage(2);
        
        Drug d5 = drugCatalog.addDrug();
        d5.setDrugName("Hydrochlorothiazide");
        d5.setDrugColor("White");
        d5.setDrugShape("Oval");
        d5.setGenericName("Hydrochlorothiazide");
        d5.setManufacturer("Microzide");
        d5.setListPrice(10);
        d5.setMaxDosage(4);
        d5.setMinDosage(2);
        d5.setRecommendedDOsage(2);
        
        
        
        
        return drugCatalog;
    }
    
    
}
