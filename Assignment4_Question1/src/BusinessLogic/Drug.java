/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author m
 */
public class Drug {
    private static int count = 0;
    private String drugName;
    private String genericName;
    private int drugID;
    private String manufacturer;
    private int listPrice;
    private String drugColor;
    private String drugShape;
    private int minDosage;
    private int maxDosage;
    private int recommendedDOsage;

    public Drug() {
        count++;
        drugID = count;
    }
    
    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public int getDrugID() {
        return drugID;
    }


    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getListPrice() {
        return listPrice;
    }

    public void setListPrice(int listPrice) {
        this.listPrice = listPrice;
    }

    public String getDrugColor() {
        return drugColor;
    }

    public void setDrugColor(String drugColor) {
        this.drugColor = drugColor;
    }

    public String getDrugShape() {
        return drugShape;
    }

    public void setDrugShape(String drugShape) {
        this.drugShape = drugShape;
    }

    public int getMinDosage() {
        return minDosage;
    }

    public void setMinDosage(int minDosage) {
        this.minDosage = minDosage;
    }

    public int getMaxDosage() {
        return maxDosage;
    }

    public void setMaxDosage(int maxDosage) {
        this.maxDosage = maxDosage;
    }

    public int getRecommendedDOsage() {
        return recommendedDOsage;
    }

    public void setRecommendedDOsage(int recommendedDOsage) {
        this.recommendedDOsage = recommendedDOsage;
    }

    @Override
    public String toString() {
        return drugName;
    }
}
