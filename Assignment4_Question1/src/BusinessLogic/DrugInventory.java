/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author m
 */
public class DrugInventory {
    
    private ArrayList<DrugItem> drugInvetoryList;
    
    public DrugInventory(){
        drugInvetoryList = new ArrayList<DrugItem>();
    }

    public ArrayList<DrugItem> getDrugInvetoryList() {
        return drugInvetoryList;
    }

    public void setDrugInvetoryList(ArrayList<DrugItem> drugInvetoryList) {
        this.drugInvetoryList = drugInvetoryList;
    }
    
    
    public DrugItem addDrugItem(){
        DrugItem di = new DrugItem();
        drugInvetoryList.add(di);
        return di;
    }
    public void removeDrugItem(DrugItem di){
        drugInvetoryList.remove(di);
    }
    
}
    
    
