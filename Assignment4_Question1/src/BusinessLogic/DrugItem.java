/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.Date;

/**
 *
 * @author m
 */
public class DrugItem {
    private int serialNum;
    private Date expiryDate;
    private int quantity;
    private Drug drug;
    private static int count;

    public DrugItem() {
        count++;
        serialNum = count;
    }
    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }
    
    
    public int getSerialNum() {
        return serialNum;
    }

    

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return drug.getDrugName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
