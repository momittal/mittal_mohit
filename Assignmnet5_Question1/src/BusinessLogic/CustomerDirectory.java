/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author m
 */
public class CustomerDirectory {
    private ArrayList<Customer> customerlist;

    public CustomerDirectory() {
        customerlist = new ArrayList<Customer>();
    }

    public ArrayList<Customer> getCustomerlist() {
        return customerlist;
    }
    
    public Customer addCustomer(){
        Customer c = new Customer();
        customerlist.add(c);
        return c;
    }
    
    public void removeCustomer(Customer c){
        customerlist.remove(c);
    }
}
