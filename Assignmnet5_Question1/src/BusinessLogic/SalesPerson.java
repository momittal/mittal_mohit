/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author m
 */
public class SalesPerson {
    private String salesPersonName;
    private int salesPersonID;
    private double commissionPercent;
    private double totalCommissionEarned;
    private static int count = 0;
    private ArrayList<Order> orderList;
    
    public SalesPerson(){
        count++;
        salesPersonID = count;
        orderList = new ArrayList<Order>();
    }
    
    
    public Order addOrder(Order o){        
        orderList.add(o);
        return o;
    }

    public ArrayList<Order> getOrderlist() {
        return orderList;
    }

    public void setOrderlist(ArrayList<Order> orderlist) {
        this.orderList = orderlist;
    }
    
    
    
    public String getSalesPersonName() {
        return salesPersonName;
    }

    public void setSalesPersonName(String salesPersonName) {
        this.salesPersonName = salesPersonName;
    }

    public int getSalesPersonID() {
        return salesPersonID;
    }

    public void setSalesPersonID(int salesPersonID) {
        this.salesPersonID = salesPersonID;
    }

    public double getCommissionPercent() {
        return commissionPercent;
    }

    public void setCommissionPercent(double commissionPercent) {
        this.commissionPercent = commissionPercent/100;
    }

    public double getTotalCommissionEarned() {
        return totalCommissionEarned;
    }

    public void setTotalCommissionEarned(double totalCommissionEarned) {
        this.totalCommissionEarned = totalCommissionEarned;
    }

    

    @Override
    public String toString() {
        return salesPersonName;
    }
    
    
    
    
}
