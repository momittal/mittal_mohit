/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author m
 */
public class Product {
    private String productName;
    private int productID;
    private int maxPrice;
    private int minPrice;
    private int targetPrice;
    private int availability;
    private static int count=0;
    
    public Product() {
    count++;
    productID = count;
    }

    public int getProductID() {
        return productID;
    }
    
    
    
    public String getProductName() {
        return productName;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }
    
    
    
    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(int targetPrice) {
        this.targetPrice = targetPrice;
    }

    public int getAvailability() {
        return availability;
    }

    public void setAvailability(int availability) {
        this.availability = availability;
    }

    @Override
    public String toString() {
        return productName;
    }
    
    
    
    
}
