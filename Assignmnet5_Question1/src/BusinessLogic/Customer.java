/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author m
 */
public class Customer {
    private String customerName;
    private int customerId;
    private static int count = 0;
    private ArrayList<Order> orderList;

    public Customer() {
        count++;
        customerId = count;
        orderList = new ArrayList<Order>();
    }
    public Order addOrder(Order o) {        
        orderList.add(o);
        return (o);
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public ArrayList<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }
    
    
    
    @Override
    public String toString() {
        return customerName; //To change body of generated methods, choose Tools | Templates.
    }
    
}
