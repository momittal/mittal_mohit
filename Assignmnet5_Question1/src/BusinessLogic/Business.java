/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author m
 */
public class Business {
    private static Business business;
    private MasterOrderCatalog moc;
    private ProductCatalog productCatalog;
    private SalesPersonDirectory salesPersonDirectory;
    private CustomerDirectory customerdirectory;
    
    private Business() {
        moc = new MasterOrderCatalog();
        productCatalog = new ProductCatalog();
        salesPersonDirectory = new SalesPersonDirectory();
        customerdirectory = new CustomerDirectory();
    }
    
    public static Business getInstance() {
        if (business == null) {
            business = new Business();
        }

        return business;
    }

    public MasterOrderCatalog getMoc() {
        return moc;
    }

    public void setMoc(MasterOrderCatalog moc) {
        this.moc = moc;
    }

    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }

    public SalesPersonDirectory getSalesPersonDirectory() {
        return salesPersonDirectory;
    }

    public void setSalesPersonDirectory(SalesPersonDirectory salesPersonDirectory) {
        this.salesPersonDirectory = salesPersonDirectory;
    }

    public CustomerDirectory getCustomerdirectory() {
        return customerdirectory;
    }

    public void setCustomerdirectory(CustomerDirectory customerdirectory) {
        this.customerdirectory = customerdirectory;
    }
    
    
    
    
}
