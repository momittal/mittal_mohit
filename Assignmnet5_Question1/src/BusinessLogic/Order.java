/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author m
 */
public class Order {
 
    private static int count = 0;
    private ArrayList<OrderItem> orderItemList;
    private int orderID;

     public Order() {
        count++;
        orderID = count;
        orderItemList = new ArrayList<OrderItem>();

    }
    
    
    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }
    
    public OrderItem addOrderItem(Product prod, int quant, int salesPrice) {
        OrderItem orderItem = new OrderItem();
        orderItem.setProduct(prod);
        orderItem.setQuantity(quant);
        orderItem.setSalesprice(salesPrice);
        orderItemList.add(orderItem);
        return orderItem;
    }
    
    
    public void removeOrderItem(OrderItem orderItem) {
        orderItemList.remove(orderItem);
    }
    
}
