/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author m
 */
public class ConfigureBusiness {
    public static Business initializeBusiness() {
        Business business = Business.getInstance();
        Product p1 = business.getProductCatalog().addProduct();
        p1.setProductName("Phaser 3260");
        p1.setAvailability(500);
        p1.setMaxPrice(500);
        p1.setMinPrice(200);
        p1.setTargetPrice(300);

        Product p2 = business.getProductCatalog().addProduct();
        p2.setProductName("Phaser 3320");
        p2.setProductID(2);
        p2.setAvailability(1000);
        p2.setMaxPrice(300);
        p2.setMinPrice(200);
        p2.setTargetPrice(250);

        Product p3 = business.getProductCatalog().addProduct();
        p3.setProductName("Phaser 3610");
        p3.setAvailability(1000);
        p3.setMaxPrice(1000);
        p3.setMinPrice(500);
        p3.setTargetPrice(700);
        
        Product p4 = business.getProductCatalog().addProduct();
        p4.setProductName("Phaser 4622");
        p4.setAvailability(1000);
        p4.setMaxPrice(600);
        p4.setMinPrice(350);
        p4.setTargetPrice(500);
        
        Product p5 = business.getProductCatalog().addProduct();
        p5.setProductName("Phaser 6022");
        p5.setAvailability(1000);
        p5.setMaxPrice(600);
        p5.setMinPrice(350);
        p5.setTargetPrice(500);
        
        SalesPerson sp1 = business.getSalesPersonDirectory().addSalesPerson();
        sp1.setSalesPersonName("John Doe");
        sp1.setCommissionPercent(10);
        
        SalesPerson sp2 = business.getSalesPersonDirectory().addSalesPerson();
        sp2.setSalesPersonName("Jack");
        sp2.setCommissionPercent(20);
        
        SalesPerson sp3 = business.getSalesPersonDirectory().addSalesPerson();
        sp3.setSalesPersonName("Mohit");
        sp3.setCommissionPercent(25);
        
        SalesPerson sp4 = business.getSalesPersonDirectory().addSalesPerson();
        sp4.setSalesPersonName("Mike");
        sp4.setCommissionPercent(20);
        
        Customer c1 = business.getCustomerdirectory().addCustomer();
        c1.setCustomerName("NEU");
        
        Customer c2 = business.getCustomerdirectory().addCustomer();
        c2.setCustomerName("MIT");
        
        Customer c3 = business.getCustomerdirectory().addCustomer();
        c3.setCustomerName("GOOGLE");
        
        Customer c4 = business.getCustomerdirectory().addCustomer();
        c4.setCustomerName("IBM");
        
        Customer c5 = business.getCustomerdirectory().addCustomer();
        c5.setCustomerName("INTEL");
        
        Customer c6 = business.getCustomerdirectory().addCustomer();
        c6.setCustomerName("AMAZON");
        
        return business;
        
        
        
    }
}
